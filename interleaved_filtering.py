"""Find the condorcet winner in a PB-MAB problem with Interleaved Filtering."""

from typing import Callable
from typing import Tuple

import numpy as np


class InterleavedFiltering:
    """Implements the Interleaved Filtering algorithm.

    This is an explore-then-exploit algorithm assuming a total order over arms, strong stochastic transitivity,
    and the stochastic triangle inequality. The Interleaved Filtering algorithm gives the Condorcet winner which is the best arm in the provided set of arms.
    The algorithm is explained in [1]_.

    Exploration:-
    The Interleaved Filtering follows a sequential elimination approach in the exploration phase and thereby
    finds the best arm with probability at least 1 - delta. At each time step, the algorithm
    selects an arm and compares it with all the other arms in a one-versus-all manner.
    If the algorithm selects an arm "a" (candidate arm), then it compares all the other arms with "a". If there exists any arm, "b"
    such that upper confidence bound of "a" beating "b" is less than 1/2, then arm "a" is eliminated and arm "b" becomes
    the candidate arm and is compared to all other active arms. It applies a pruning technique to
    eliminate arm "b" if the lower confidence bound of "a" beating "b" is greater than 1/2,
    as it cannot be considered as the best arm. After the exploration, the candidate arm and the total number of comparisons are given as output.
    It is possible that total number of comparisons is greater than the given time horizon.

    Exploitation:-
    If the total number of comparisons is less than the given time horizon then the algorithm enters into the exploitation phase.
    In the exploitation phase, only the best arm from the exploration phase is pulled and compared to itself, assuming that the exploration found the best arm.

    Parameters
    ----------
    time_horizon
        For how many time steps the algorithm should run (time horizon greater than or equal to number of arms).
    arms
        The set of arms available to the algorithm.
    feedback
        A function for comparing two arms. It takes two arms as parameters and returns 1 if first arm wins and -1 otherwise.

    Attributes
    ----------
    feedback
        The feedback function passed as a parameter.
    failure_probability
        Allowed failure-probability, i.e. probability that the actual value lies outside of the computed confidence interval.
        Derived from the Hoeffding bound.
    candidate_arm
        Randomly selected arm from the list of arms.
    arms_without_candidate
        The remaining set of arms after removing the candidate arm.
    num_wins
        Number of times that candidate_arm wins over the other arms in the remaining list of arms, after removing
        the candidate arm.
    probability_estimate
        Estimated preference probability of the candidate arm against all other arms.
    confidence_bounds
        Upper and lower confidence bounds on the preference probability of the candidate arm against all other arms.
    num_comparisons_with_candidate
        Number of comparisons made between candidate arm and rest of the arms.


    References
    ----------
    .. [1] Yisong Yue, Josef Broder, Robert Kleinberg, and Thorsten Joachims. The K-armed Dueling Bandits Problem.
    Journal of Computer and System Sciences, 78(5):1538–1556, 2012.

    """

    # pylint: disable=too-many-instance-attributes
    def __init__(
        self,
        time_horizon: int,
        arms: list,
        feedback: Callable[[int, int], int],
        random_state: np.random.RandomState = np.random.RandomState(),
    ) -> None:
        self.time_horizon = time_horizon
        self.feedback = feedback
        self.failure_probability = 1 / (time_horizon * (len(arms) ** 2))
        self.candidate_arm = random_state.choice(arms)
        self.arms_without_candidate = arms.copy()
        self.arms_without_candidate.remove(self.candidate_arm)
        self.num_wins = np.zeros((len(self.arms_without_candidate),))
        self.probability_estimate = np.zeros((len(self.arms_without_candidate),))
        self.confidence_bounds = np.zeros((len(self.arms_without_candidate), 2))
        self.num_comparisons_with_candidate = 0

    def interleaved_filter(self) -> int:
        """Find the condorcet winner using Interleaved Filtering.

        Returns
        -------
        candidate_arm
           The condorcet winner in the set of arms given to the algorithm.
        """
        candidate_arm, total_comparisons = self.explore()
        if total_comparisons < self.time_horizon:
            self.exploit(total_comparisons)
        return candidate_arm

    def explore(self) -> Tuple[int, int]:  # pylint: disable=too-many-locals
        """Exploration phase of the Interleaved Filtering algorithm to find the candidate arm, which is the condorcet winner.

        Returns
        -------
        candidate_arm
           The condorcet winner in the set of arms given to the algorithm.
        total_comparisons
           The total number of comparisons that are made in finding the condorcet winner.
        """
        total_comparisons = 0
        while len(self.arms_without_candidate) != 0:
            self.num_comparisons_with_candidate += 1
            # The confidence_radius used for construction of confidence bounds, calculated by
            # using the formula derived from Hoeffdings inequality.
            confidence_radius = np.sqrt(
                (4 * np.math.log(1 / self.failure_probability))
                / self.num_comparisons_with_candidate
            )
            # The confidence radius is the same for every arm (as each arm is pulled in each iteration) and
            # it is calculated after the comparison counter is incremented.
            for index, arm in enumerate(self.arms_without_candidate):
                if self.feedback(self.candidate_arm, arm) == 1:
                    self.num_wins[index] += 1
                self.probability_estimate[index] = (
                    self.num_wins[index] / self.num_comparisons_with_candidate
                )
                self.confidence_bounds[index][0] = (
                    self.probability_estimate[index] - confidence_radius
                )
                self.confidence_bounds[index][1] = (
                    self.probability_estimate[index] + confidence_radius
                )
                total_comparisons += 1

            updated_arms_without_candidate = self.prune_arms()
            (self.arms_without_candidate) = self.find_candidate_arm(
                updated_arms_without_candidate
            )

        return self.candidate_arm, total_comparisons

    def prune_arms(self) -> list:
        """Eliminate arms that cannot be expected to win against the candidate within the confidence interval.

        Returns
        -------
        updated_arms_without_candidate
           The remaining set of arms after eliminating all the arms which, do not satisfy the condition.
        """
        # A duplicate list of arms without candidate arm, in order to avoid the index out of bounds error while
        # removing an arm from the arms_without_candidate.
        duplicate_arms_without_candidate = np.copy(self.arms_without_candidate)
        for index, arm in enumerate(duplicate_arms_without_candidate):
            # check whether probability_estimate is greater than 1/2 AND 1/2 is not in the confidence_bounds.
            if self.confidence_bounds[index][0] > 1 / 2:
                self.arms_without_candidate.remove(arm)
        updated_arms_without_candidate = self.arms_without_candidate
        return updated_arms_without_candidate

    def find_candidate_arm(self, updated_arms_without_candidate: list) -> list:
        """Find the candidate arm and remove the new candidate arm from the list of updated arms without candidate arm.

        Parameters
        ----------
        updated_arms_without_candidate
            The remaining set of arms after eliminating all the arms whose, lower confidence bound of candidate arm and
            each arm in the set of all arms except candidate arm is greater than 1/2.

        Returns
        -------
        updated_arms_without_candidate
            The updated list of arms after removing the new candidate arm.
        """
        candidate_found = False
        for index, arm in enumerate(updated_arms_without_candidate):
            # check whether, if there is any arm whose probability_estimate is less than 1/2 AND 1/2 is not in
            # the confidence_bounds.
            if self.confidence_bounds[index][1] < 1 / 2:
                self.candidate_arm = arm
                candidate_found = True
                break
        if candidate_found:
            self.arms_without_candidate = updated_arms_without_candidate
            self.arms_without_candidate.remove(self.candidate_arm)
            self.reset(updated_arms_without_candidate)

        return updated_arms_without_candidate

    def reset(self, updated_arms_without_candidate: list) -> None:
        """Reset all the variables to initial values (zero).

        Parameters
        ----------
        updated_arms_without_candidate
            The updated list of arms after removing the new candidate arm.
        """
        self.probability_estimate = np.zeros((len(updated_arms_without_candidate),))
        self.confidence_bounds = np.zeros((len(updated_arms_without_candidate), 2))
        self.num_wins = np.zeros((len(updated_arms_without_candidate),))
        self.num_comparisons_with_candidate = 0

    def exploit(self, total_comparisons: int) -> None:
        """Exploitation phase of the Interleaved Filtering algorithm, repeatedly choosing the condorcet winner by comparing with itself.

        Parameters
        ----------
        total_comparisons
           The total number of comparisons that are made in finding the condorcet winner.
        """
        candidate_arm = self.candidate_arm
        for _ in range(total_comparisons + 1, self.time_horizon):
            if candidate_arm == self.candidate_arm:
                print("EXPLORE phase found the condocert winner")


def _main() -> None:
    """Test the algorithm implementation.

    As we have no benchmarking/environment packages yet, a specific example is used here.
    """
    # arm 1 should be the best arm, the arms can also be ranked as 1,2,3,4
    preference_matrix = np.array(
        [
            [0.5, 0.85, 0.85, 0.85],
            [0.15, 0.5, 0.70, 0.70],
            [0.15, 0.30, 0.5, 0.55],
            [0.15, 0.30, 0.45, 0.5],
        ]
    )

    def feedback(i: int, j: int) -> int:
        """Compare two arms and gives feedback.

        Parameters
        ----------
        i
           The first arm.
        j
           The second arm.

        Returns
        -------
        result
           The feedback of comparison i.e, result will be 1 if the first arm wins or -1 if the second arm wins.
        """
        rand = np.random.uniform()
        winner = i if rand < preference_matrix[i, j] else j
        result = 1 if winner == i else -1

        return result

    time_horizon = 7  # time horizon greater than or equal to number of arms.
    arms = list(range(len(preference_matrix)))
    interleaved_filtering = InterleavedFiltering(time_horizon, arms, feedback)
    condorcet_winner = interleaved_filtering.interleaved_filter()
    print(
        "Condorcet winner is the arm at index",
        condorcet_winner,
        "->",
        "arm",
        int(condorcet_winner) + 1,
        ".",
    )


if __name__ == "__main__":
    _main()
