"""An over-engineered greetings framework.

Can you believe how over-engineered this is? Still, generating greetings is a
common and tedious task, so you can use this module to make it a little
simpler.
"""


def make_greeting(greeter: str, greetee: str = "world") -> str:
    """
    Generates a friendly greeting.

    This will generate a greeting string from `greeter` to `greetee`.

    This is a small docstring example formatted in numpydoc__, which is also
    used by scipy and scikit-learn. We can have our :math:`\pi` and eat it,
    too!

    __ https://numpydoc.readthedocs.io/en/latest/format.html

    Parameters
    ----------
    greeter
        The person issuing the greeting.
    greetee
        The person or entity being greeted.

    Returns
    -------
    str
        A friendly greeting string.

    Notes
    -----
    This is incredibly over-engineered. You may simply generate the string
    yourself instead.

    References
    ----------
    There is some barely relevant literature [1]_ that I haven't ready myself.
    Note that the underscore creates a link in re-structured text, which is the
    format this docstring is written in.  That makes it possible to build
    good-looking HTML docs. Check out this
    `Example <https://numpydoc.readthedocs.io/en/latest/example.html#module-example>`_.
    I have simply copied the "MLA" citation from some paper in google scholar
    that turned up when searching for "Hello, world".

    .. [1] Westfall, Ralph. "Technical opinion: Hello, world considered
       harmful." Communications of the ACM 44.10 (2001): 129-130.

    Examples
    --------
    You can use this function to generate greetings more efficiently:

    >>> make_greeting("Timo")
    Hello, world! Greetings from Timo!

    isn't that nice? You can greet other things too!

    >>> make_greeting("the duelpy team", "GitLab")
    Hello, GitLab! Greetings from the duelpy team!
    """
    return f"Hello, {greetee}! Greetings from {greeter}!"
