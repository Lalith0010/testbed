"""Implements greetings from Timo"""

from greetings import make_greeting


def greetings_from_timo():
    """Prints greetings from Timo.

    There isn't really anything else to say. Note that this will print text to
    the terminal as a side-effect.

    See Also
    --------
    greetings.make_greeting : This just just a thin wrapper around that function.
    """
    print(make_greeting("Timo"))


if __name__ == "__main__":
    greetings_from_timo()
